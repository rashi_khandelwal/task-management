from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import View
from .models import Task
from .models import Comment
from .models import Org
from .models import User
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from datetime import datetime
import json
from django.http import JsonResponse
from django.http import QueryDict
from django.core.exceptions import *
from django.db import *
#To get the list of tasks and their status
class tasks(View):

	def get(self, request):
		print 'calling tasks'
		_params = request.GET
		#_list is a dictionary to access the status TODO DOING DONE with their respective keys
		t_todo = Task.objects.filter(_list = 1)
		t_doing = Task.objects.filter(_list = 2)
		t_done = Task.objects.filter(_list = 3)

		#Displays the list
		output1 = ', '.join(t.title for t in t_todo)
		output2 = ', '.join(t.title for t in t_doing)
		output3 = ', '.join(t.title for t in t_done)

		return HttpResponse("The tasks listed by status TODO are " + output1 + " DOING are " + output2 + " DONE are " + output3)

#Creation of task
class create_tasks(View):

	def post(self, request):
		print 'calling create tasks'
		_params = request.POST
		#Reading the basic parameters from body
		title = _params.get('title')
		description = _params.get('description')
		due_date = _params.get('due_date')
		label = _params.get('label')
		_list = _params.get('_list')
		created_by = _params.get('created_by')


		com_create = Comment.objects.create(created_by= 'a' , comment_text = 'aa' )
		#Creating Task object
		output = Task.objects.create(title = title, description = description, due_date=due_date, label =label , _list=_list, comment=com_create , created_by=created_by)
		output.save()
		return HttpResponse("Task with title " +title+ " created.")

#To view the details
class details(View):
	#Returns the description according to the tasks title
	def get(self, request, title):
		print 'calling details'
		task_title = title
		_params = request.GET
		li = Task.objects.filter(title = task_title)
		output = ', '.join(t.description for t in li)
		return HttpResponse("The task description for the task " + task_title + " is: " + output)

	#Updates the attributes of task with the entered title
	def put(self, request, title):
		print 'calling edit tasks'
		title1 = title
		_params = request.body
		#print _params
		#title = _params.get('title')
		description = _params.get('description')
		print description
		due_date = _params.get('due_date')
		label = _params.get('label')
		_list = _params.get('_list')
		created_by = _params.get('created_by')

		com_create = Comment.objects.create(created_by= 'a' , comment_text = 'aa' )
		output = Task.objects.filter(title = title1)
		output.update(description = description, due_date=due_date, label =label , _list=_list, comment=com_create , created_by=created_by)
		output.save()
		return HttpResponse("Task with title " + title + " edited.")

	#Deletes the task with the given title name
	def delete(self, request, title):
		print 'calling delete tasks'
		title1= title
		_params = request.GET
		#input = _params.get('input')
		to_delete = Task.objects.filter(title = title1)
		to_delete.delete()
		#to_delete.save()
		return HttpResponse("Task with title "+ title1 + " deleted.")


#API to create org object, get the details and update org object
class org(View):

	#This creates an org object
	def post(self, request):
		print 'calling create org'
		_params = request.POST

		org_id = _params.get('org_id')
		address = _params.get('address')
		gstin_no = _params.get('gstin_no')
		if gstin_no[13] != 'Z' or len(gstin_no)!=15:
			return HttpResponse("Enter a valid gstin number")
		else:

			basic_currency = _params.get('basic_currency')
			if not org_id or not address or not gstin_no or not basic_currency:
				return HttpResponse("Please enter all the fields.")

			else:
				try:
					org_obj = Org.objects.create(org_id= org_id, address=address, gstin_no = gstin_no, basic_currency=basic_currency)
					org_obj.save()
					return HttpResponse("Organization with organization id : "+org_id+ " created.")
				except IntegrityError:
					return HttpResponse("The organization id is already in use. Try some different id.")

	#This returns the details of a particular org bearing the provided org_id
	def get(self, request):
		#import pdb;pdb.set_trace()
		print 'calling get org'
		params = request.GET
		try:
			id_list = params.get('id_list')
			id_list = eval(id_list)
			org_out = Org.objects.filter(org_id__in = id_list).values()
			#res = [obj.as_dict() for obj in org_out]
			return JsonResponse({"Organization list " : list(org_out)})
		except TypeError:
			return HttpResponse("Enter the id_list in the form of a list.")

	#Updates the details of that particular org with org_id=input_id
	def put(self, request, input_id):
		print 'calling put'
		input_id = input_id

		params = QueryDict(request.body)
		address = params.get('address')
		gstin_no = params.get('gstin_no')
		if gstin_no[13] != 'Z' or len(gstin_no)!=15:
			return HttpResponse("Enter a valid gstin number")
		else:
			basic_currency = params.get('basic_currency')
			if basic_currency not in ['INR', 'USD', 'EUR', 'AED', 'SGR']:
				return HttpResponse("Enter 1 of the five given basic_currency.")
			else:
				if not address or not gstin_no or not basic_currency:
					return HttpResponse("Please enter all the fields.")
				else:
					org_obj = Org.objects.filter(org_id= input_id)
					for objt in org_obj:
						objt.address = address
						objt.gstin_no = gstin_no
						objt.basic_currency = basic_currency
						objt.save()

					out = Org.objects.filter(org_id = input_id).values()
					return JsonResponse({"Updated : " : list(out)})


#This API create user object, get the details and update user object
class user(View):

	#This creates a user object
	def post(self, request):
		#import pdb;pdb.set_trace()
		print 'calling post user'
		_params = request.POST

		user_id = _params.get('user_id')
		email_id = _params.get('email_id')
		if '@' not in email_id: 
			return HttpResponse("Enter a valid email_id.")
		else:
			phone_no = _params.get('phone_no')
			if len(phone_no) != 10:
				return HttpResponse("Enter a valid phone number.")
			else:
				org_id = _params.get('org_id')
				if not user_id or not email_id or not phone_no or not org_id:
					return HttpResponse("Please enter all the fields.")
				else:
					try:
						user_obj = User.objects.create(user_id  =user_id , email_id = email_id, phone_no = phone_no, org_id = org_id)
						user_obj.save()

						return HttpResponse("User with ID " + user_id + " created.")
					except IntegrityError:
						return HttpResponse("The user id is already in use. Please enter a valid user id.")

	#This returns the details of a particular org bearing the provided user_id
	def get(self, request):
		#import pdb;pdb.set_trace()
		print 'calling get user'
		try:
			params = request.GET
			id_list = params.get('id_list')
			id_list = eval(id_list)

			user_out = User.objects.filter(org_id__in = id_list).values()
			#res = [obj.as_dict() for obj in org_out]
			return JsonResponse({"User list " : list(user_out)})
		except TypeError:
			return HttpResponse("Enter the id_list in the form of a list.")

	#Updates the details of that particular user with user_id=input_id
	def put(self, request, input_id):
		print 'calling put'
		input_id = input_id
		params = QueryDict(request.body)
		email_id = params.get('email_id')
		if '@' not in email_id: 
			return HttpResponse("Enter a valid email_id.")
		else:
			phone_no = params.get('phone_no')
			if len(phone_no) != 10:
				return HttpResponse("Enter a valid phone number.")
			else:
				org_id = params.get('org_id')
				if not user_id or not email_id or not phone_no or not org_id:
					return HttpResponse("Please enter all the fields.")
				else:
					user_obj = User.objects.filter(user_id= input_id)
					for objt in user_obj:
						objt.email_id = email_id
						objt.phone_no = phone_no
						objt.org_id = org_id
						objt.save()

					out = User.objects.filter(user_id = input_id).values()
					return JsonResponse({"Updated : " : list(out)})




