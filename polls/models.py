from django.db import models
from datetime import datetime
from django.core.validators import RegexValidator
from django.core.validators import MinLengthValidator
from django.core.validators import MaxLengthValidator
from django.contrib.postgres.fields import JSONField
#from phonenumber_field.modelfield import PhoneNumber

#Choices for labrl colors
COLOR_CHOICES = [
	('V' , 'Violet'),
	('I' , 'Indigo'),
	('B' , 'Blue'),
	('G' , 'Green'),
	('Y' , 'Yellow'),
	('O' , 'Orange'),
	('R' , 'Red'),
]

#Choices for tasks status
TASK_STATUS = [
	('1' , 'TODO'),
	('2' , 'DOING'),
	('3' , 'DONE'),
]

#Currency Choices
CURRENCY_CHOICES = [
	('INR' , 'Indian Rupee'),
	('USD' , 'US Dollar'),
	('EUR' , 'Eurozone'),
	('AED' , 'Arab Emirates Dirham'),
	('SGR' , 'Singapore Dollar'),
]

class Comment(models.Model):
	created_by = models.CharField(max_length = 100, default = 'a')
	created_on = models.DateField(default = datetime.now())
	updated_on = models.DateField(default = datetime.now())
	comment_text = models.TextField( default = 'a')

	def __str__(self):
		return self.comment_text

class Task(models.Model):

	title = models.CharField(max_length =200)
	description = models.CharField(max_length = 1000 , default = 'a')
	due_date = models.DateField(default = datetime.now())
	label = models.CharField(max_length =1, choices = COLOR_CHOICES , default  = 'G')
	attachment = models.FileField(blank = True, null = True, upload_to = "uploads/%Y/%m/%D")
	_list = models.CharField(max_length = 1, default = 1 , choices = TASK_STATUS)
	comment = models.ForeignKey(Comment, on_delete = models.CASCADE)
	created_by = models.CharField(max_length= 100, default = 'a')
	
	def __str__(self):
		return self.title

class Org(models.Model):

	org_id = models.IntegerField(primary_key = True)
	address = models.TextField(default = 'Address')
	gstin_no = models.CharField(max_length = 15, validators = [RegexValidator(regex = '^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$', message = 'Enter a valid GST number!' )])
	basic_currency = models.CharField(max_length = 3, choices = CURRENCY_CHOICES, default = 'INR')

	def as_dict(self):
		
		return {
				"org_id" : self.org_id,
				"address" : self.address,
				"gstin_no" : self.gstin_no,
				"basic_currency" : self.basic_currency,
		}

class User(models.Model):
	#, validators = [RegexValidator(regex = '^[0-9]{10}$', message = 'Enter a valid 10 digit phone number!')]
	user_id = models.IntegerField(primary_key = True)
	email_id = models.EmailField(max_length = 254, unique = True)
	phone_no = models.BigIntegerField(validators=[MaxLengthValidator(6),MinLengthValidator(6)])
	org = models.ForeignKey(Org, on_delete = models.CASCADE)
