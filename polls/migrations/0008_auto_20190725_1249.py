# -*- coding: utf-8 -*-
# Generated by Django 1.11.22 on 2019-07-25 12:49
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0007_auto_20190725_1238'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='extra_details',
        ),
        migrations.AlterField(
            model_name='comment',
            name='created_on',
            field=models.DateField(default=datetime.datetime(2019, 7, 25, 12, 49, 52, 570919)),
        ),
        migrations.AlterField(
            model_name='comment',
            name='updated_on',
            field=models.DateField(default=datetime.datetime(2019, 7, 25, 12, 49, 52, 570958)),
        ),
        migrations.AlterField(
            model_name='task',
            name='due_date',
            field=models.DateField(default=datetime.datetime(2019, 7, 25, 12, 49, 52, 581218)),
        ),
    ]
