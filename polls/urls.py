from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from .views import *

urlpatterns = [
	#API FOR TASKS
    url(r'^tasks/$', tasks.as_view(), name='tasks'),
    url(r'^tasks/$' , csrf_exempt(create_tasks.as_view()) , name = 'create_tasks'),
    url(r'^tasks/(?P<title>.*)/$', details.as_view(), name='details'),

   #API FOR ORG
    url(r'^org/$' , csrf_exempt(org.as_view()), name = 'org create'),
    url(r'^org/$' , org.as_view(), name = 'get org'),
    url(r'^org/(?P<input_id>.*)/$' , csrf_exempt(org.as_view()), name = 'update org'),
    
    #API FOR USER
    url(r'^user/$' , csrf_exempt(user.as_view()), name = 'user create'),
    url(r'^user/$' , user.as_view(), name = 'get user'),
    url(r'^user/(?P<input_id>.*)/$' , csrf_exempt(user.as_view()), name = 'update user'),
]
