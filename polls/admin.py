from django.contrib import admin

from .models import Comment
from .models import Task
from .models import Org
from .models import User

admin.site.register(Comment)
admin.site.register(Task)
admin.site.register(Org)
admin.site.register(User)